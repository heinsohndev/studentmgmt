# STUDENT-MGMT

##Installation
1. Projekt mit Git klonen
2. Tomcat installieren (http://tomcat.apache.org/download-70.cgi Service Installer auswählen)
3. Maven installieren (https://www.mkyong.com/maven/how-to-install-maven-in-windows/) Java SDK 1.7 und Java JRE 1.7 installieren! Ansonsten kann es Probleme beim Aufruf der Webseite geben
4. H2 installieren (http://www.h2database.com/h2-setup-2017-06-10.exe)
5. Deployment-Skript ausführen (Wichtig: Als Administrator ausführen!)

##Anwendung erstellen
Damit die Anwendung richtig zur Verfügung gestellt werden kann, muss vor dem Erstellvorgang der Tomcat-Dienst gestartet werden.
Um die Anwendung zu erstellen, müssen folgende Maven-Funktionen ausgeführt werden.
1. mvn clean:clean
2. mvn compiler:compile
3. mvn war:war
4. mvn cargo:deploy

Alternativ kann das Erstellen auch durch das Deploymentskript 'Deploy.ps1' ausgeführt werden.

##Anwendung starten
Die Anwendung kann nach dem Erstellen im Browser unter der Adresse 'http://localhost:8080/studentmgmt' aufgerufen werden. 