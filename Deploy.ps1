﻿mvn clean:clean
mvn compiler:compile

$executingScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
copy-item $executingScriptDirectory\src\main\resources\struts.xml $executingScriptDirectory\target\classes

mvn war:war
mvn cargo:deploy