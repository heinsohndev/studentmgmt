package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.AcademicYearDAO;
import de.nordakademie.iaa.studentmgmt.model.AcademicYear;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for AcademicYearService
 */
public class AcademicYearService {
    @Autowired
    private AcademicYearDAO academicYearDAO;

    /**
     * List of all academicYears that are currently in the database
     *
     * @return a list of all academicYears that are currently in the database
     */
    public List<AcademicYear> getAll() {
        return academicYearDAO.getAll();
    }

    /**
     * Returns the academicYear identified by the given year
     *
     * @param year the identifier
     * @return the found entity
     */
    public AcademicYear getOne(Short year){
        return academicYearDAO.getOne(year);
    }

    /**
     * Stores academicYear into database
     *
     * @param academicYear The academicYear to be created
     */
    public void create(AcademicYear academicYear){
        academicYearDAO.create(academicYear);
    }

    /**
     * Deletes given academicYear
     *
     * @param year The academicYear to be deleted
     */
    public void delete(Short year){
        AcademicYear academicYear = getOne(year);
        academicYearDAO.delete(academicYear);
    }

    /**
     * Updates given academicYear
     *
     * @param academicYear The academicYear to be updated
     */
    public void update(AcademicYear academicYear){
        academicYearDAO.update(academicYear);
    }


}
