package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.StudentDAO;
import de.nordakademie.iaa.studentmgmt.model.Centuria;
import de.nordakademie.iaa.studentmgmt.model.Manipel;
import de.nordakademie.iaa.studentmgmt.model.Status;
import de.nordakademie.iaa.studentmgmt.model.Student;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for StudentService
 */
public class StudentService {
    @Autowired
    private StudentDAO studentDAO;

    /**
     * List of all students that are currently in the database
     *
     * @return a list of all students that are currently in the database
     */
    public List<Student> getAll() {
        return studentDAO.getAll();
    }

    public List<Student> getAllByManipel(Manipel manipel) {
        return studentDAO.getAllByManipel(manipel);
    }

    public List<Student> getAllByCenturia(Centuria centuria) {
        return studentDAO.getAllByCenturia(centuria);
    }

    /**
     * Returns the student identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Student getOne(Long id){
        return studentDAO.getOne(id);
    }

    /**
     * Stores student into database
     *
     * @param student The student to be created
     */
    public void create(Student student){
        studentDAO.create(student);
    }

    /**
     * Deletes given student
     *
     * @param id The student to be deleted
     */
    public void exmatriculate(Long id){
        Student student = getOne(id);
        student.setStatus((short)2);
        studentDAO.update(student);
    }

    /**
     * Updates given student
     * @param student The student to be created
     */
    public void update(Student student){
        studentDAO.update(student);
    }

    public List<Status> getAllStatus() {
        List<Status> retVal = new ArrayList<>();
        Status im = new Status(1, "Immatrikuliert");
        Status ex = new Status(2, "Exmatrikuliert");
        retVal.add(im);
        retVal.add(ex);
        return retVal;
    }
}
