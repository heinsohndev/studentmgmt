package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.ContactPersonDAO;
import de.nordakademie.iaa.studentmgmt.model.ContactPerson;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for ContactPersonService
 */
public class ContactPersonService {
    @Autowired
    private ContactPersonDAO contactPersonDAO;

    /**
     * List of all contactPersons that are currently in the database
     *
     * @return a list of all contactPersons that are currently in the database
     */
    public List<ContactPerson> getAll() {
        return contactPersonDAO.getAll();
    }

    /**
     * Returns the contactPerson identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public ContactPerson getOne(Long id){
        return contactPersonDAO.getOne(id);
    }

    /**
     * Stores contactPerson into database
     *
     * @param contactPerson The contactPerson to be created
     */
    public void create(ContactPerson contactPerson){
        contactPersonDAO.create(contactPerson);
    }

    /**
     * Deletes given contactPerson
     *
     * @param id The contactPerson to be deleted
     */
    public void delete(Long id){
        ContactPerson contactPerson = getOne(id);
        contactPersonDAO.delete(contactPerson);
    }

    /**
     * Updates given contactPerson
     *
     * @param contactPerson The contactPerson to be updated
     */
    public void update(ContactPerson contactPerson){
        contactPersonDAO.update(contactPerson);
    }

}
