package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.CourseOfStudiesDAO;
import de.nordakademie.iaa.studentmgmt.model.CourseOfStudies;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for CourseOfStudiesService
 */
public class CourseOfStudiesService {
    @Autowired
    private CourseOfStudiesDAO courseOfStudiesDAO;

    /**
     * List of all courseOfStudies that are currently in the database
     *
     * @return a list of all courseOfStudies that are currently in the database
     */
    public List<CourseOfStudies> getAll() {
        return courseOfStudiesDAO.getAll();
    }

    /**
     * Returns the courseOfStudies identified by the given description
     *
     * @param id the identifier
     * @return the found entity
     */
    public CourseOfStudies getOne(Long id){
        return courseOfStudiesDAO.getOne(id);
    }

    /**
     * Stores courseOfStudies into database
     *
     * @param courseOfStudies The courseOfStudies to be created
     */
    public void create(CourseOfStudies courseOfStudies){
        courseOfStudiesDAO.create(courseOfStudies);
    }

    /**
     * Deletes given courseOfStudies
     *
     * @param id The courseOfStudies to be deleted
     */
    public void delete(Long id){
        CourseOfStudies courseOfStudies = getOne(id);
        courseOfStudiesDAO.delete(courseOfStudies);
    }

    /**
     * Updates given courseOfStudies
     *
     * @param courseOfStudies The courseOfStudies to be updated
     */
    public void update(CourseOfStudies courseOfStudies){
        courseOfStudiesDAO.update(courseOfStudies);
    }

}
