package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.CityDAO;
import de.nordakademie.iaa.studentmgmt.model.City;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for CityService
 */
public class CityService {
    @Autowired
    private CityDAO cityDAO;

    /**
     * List of all cities that are currently in the database
     *
     * @return a list of all cities that are currently in the database
     */
    public List<City> getAll() {
        return cityDAO.getAll();
    }

    /**
     * Returns the city identified by the given postCode
     *
     * @param postCode the identifier
     * @return the found entity
     */
    public City getOne(Integer postCode){
        return cityDAO.getOne(postCode);
    }

    /**
     * Stores city into database
     *
     * @param city The city to be created
     */
    public void create(City city){
        cityDAO.create(city);
    }

    /**
     * Deletes given city
     *
     * @param postCode The city to be deleted
     */
    public void delete(Integer postCode){
        City city = getOne(postCode);
        cityDAO.delete(city);
    }

    /**
     * Updates given city
     *
     * @param city The city to be updated
     */
    public void update(City city){
        cityDAO.update(city);
    }

}
