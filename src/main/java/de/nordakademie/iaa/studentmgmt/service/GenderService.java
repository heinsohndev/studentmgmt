package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.GenderDAO;
import de.nordakademie.iaa.studentmgmt.model.Gender;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for GenderSalutionService
 */
public class GenderService {
    @Autowired
    private GenderDAO genderDAO;

    /**
     * List of all genderSalutions that are currently in the database
     *
     * @return a list of all genderSalutions that are currently in the database
     */
    public List<Gender> getAll() {
        return genderDAO.getAll();
    }

    /**
     * Returns the genderSalution identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Gender getOne(Long id){
        return genderDAO.getOne(id);
    }

    /**
     * Stores genderSalution into database
     *
     * @param gender The genderSalution to be created
     */
    public void create(Gender gender){
        genderDAO.create(gender);
    }

    /**
     * Deletes given genderSalution
     *
     * @param id The genderSalution to be deleted
     */
    public void delete(Long id){
        Gender gender = getOne(id);
        genderDAO.delete(gender);
    }

    /**
     * Updates given genderSalution
     *
     * @param gender The genderSalution to be updated
     */
    public void update(Gender gender){
        genderDAO.update(gender);
    }

}
