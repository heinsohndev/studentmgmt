package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.ManipelDAO;
import de.nordakademie.iaa.studentmgmt.model.Manipel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for ManipelService
 */
public class ManipelService {
    @Autowired
    private ManipelDAO manipelDAO;

    /**
     * List of all manipel that are currently in the database
     *
     * @return a list of all manipel that are currently in the database
     */
    public List<Manipel> getAll() {
        return manipelDAO.getAll();
    }

    /**
     * Returns the manipel identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Manipel getOne(Long id){
        return manipelDAO.getOne(id);
    }

    /**
     * Stores manipel into database
     *
     * @param manipel The manipel to be created
     */
    public void create(Manipel manipel){
        manipelDAO.create(manipel);
    }

    /**
     * Deletes given manipel
     *
     * @param id The manipel to be deleted
     */
    public void delete(Long id){
        Manipel manipel = getOne(id);
        manipelDAO.delete(manipel);
    }

    /**
     * Updates given manipel
     *
     * @param manipel The manipel to be updated
     */
    public void update(Manipel manipel){
        manipelDAO.update(manipel);
    }
}
