package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.CompanyDAO;
import de.nordakademie.iaa.studentmgmt.model.Company;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for CompanyService
 */
public class CompanyService {
    @Autowired
    private CompanyDAO companyDAO;

    /**
     * List of all companies that are currently in the database
     *
     * @return a list of all companies that are currently in the database
     */
    public List<Company> getAll() {
        return companyDAO.getAll();
    }

    /**
     * Returns the company identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Company getOne(Long id){
        return companyDAO.getOne(id);
    }

    /**
     * Stores company into database
     *
     * @param company The company to be created
     */
    public void create(Company company){
        companyDAO.create(company);
    }

    /**
     * Deletes given company
     *
     * @param id The company to be deleted
     */
    public void delete(Long id){
        Company company = getOne(id);
        companyDAO.delete(company);
    }

    /**
     * Updates given company
     *
     * @param company The company to be updated
     */
    public void update(Company company){
        companyDAO.update(company);
    }

}
