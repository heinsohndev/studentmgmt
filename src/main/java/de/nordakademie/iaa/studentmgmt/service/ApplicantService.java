package de.nordakademie.iaa.studentmgmt.service;


import de.nordakademie.iaa.studentmgmt.dao.ApplicantDAO;
import de.nordakademie.iaa.studentmgmt.model.Applicant;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for ApplicantService
 */

public class ApplicantService {
    @Autowired
    private ApplicantDAO applicantDAO;

    /**
     * List of all applicatns that are currently in the database
     *
     * @return a list of all applicants that are currently in the database
     */
    public List<Applicant> getAll() {
        return applicantDAO.getAll();
    }

    /**
     * Returns the applicant identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Applicant getOne(Long id){
        return applicantDAO.getOne(id);
    }

    /**
     * Stores Applicant into database
     *
     * @param applicant The applicant to be created
     */
    public void create(Applicant applicant){
        applicantDAO.create(applicant);
    }

    /**
     * Deletes given applicant
     *
     * @param id The applicant to be deleted
     */
    public void delete(Long id){
        Applicant applicant = getOne(id);
        applicantDAO.delete(applicant);
    }

    /**
     * Updates given applicant
     *
     * @param applicant The applicant to be updated
     */
    public void update(Applicant applicant){
        applicantDAO.update(applicant);
    }
}
