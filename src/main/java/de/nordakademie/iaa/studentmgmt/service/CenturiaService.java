package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.CenturiaDAO;
import de.nordakademie.iaa.studentmgmt.model.Centuria;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for Centuria
 */
public class CenturiaService {
    @Autowired
    private CenturiaDAO centuriaDAO;

    /**
     * List of all centurias that are currently in the database
     *
     * @return a list of all centurias that are currently in the database
     */
    public List<Centuria> getAll() {
        return centuriaDAO.getAll();
    }

    /**
     * Returns the centuria identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public Centuria getOne(Long id){
        return centuriaDAO.getOne(id);
    }

    /**
     * Stores centuria into database
     *
     * @param centuria The centuria to be created
     */
    public void create(Centuria centuria){
        centuriaDAO.create(centuria);
    }

    /**
     * Deletes given centuria
     *
     * @param id The centuria to be deleted
     */
    public void delete(Long id){
        Centuria centuria = getOne(id);
        centuriaDAO.delete(centuria);
    }

    /**
     * Updates given centuria
     *
     * @param centuria The centuria to be updated
     */
    public void update(Centuria centuria){
        centuriaDAO.update(centuria);
    }

}
