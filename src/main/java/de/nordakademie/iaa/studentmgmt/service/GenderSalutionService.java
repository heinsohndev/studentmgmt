package de.nordakademie.iaa.studentmgmt.service;

import de.nordakademie.iaa.studentmgmt.dao.GenderSalutionDAO;
import de.nordakademie.iaa.studentmgmt.model.GenderSalution;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mervan Ristemi on 17.10.2018.
 *
 * Implementation for GenderSalutionService
 */
public class GenderSalutionService {
    @Autowired
    private GenderSalutionDAO genderSalutionDAO;

    /**
     * List of all genderSalutions that are currently in the database
     *
     * @return a list of all genderSalutions that are currently in the database
     */
    public List<GenderSalution> getAll() {
        return genderSalutionDAO.getAll();
    }

    /**
     * Returns the genderSalution identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    public GenderSalution getOne(Long id){
        return genderSalutionDAO.getOne(id);
    }

    /**
     * Stores genderSalution into database
     *
     * @param genderSalution The genderSalution to be created
     */
    public void create(GenderSalution genderSalution){
        genderSalutionDAO.create(genderSalution);
    }

    /**
     * Deletes given genderSalution
     *
     * @param id The genderSalution to be deleted
     */
    public void delete(Long id){
        GenderSalution genderSalution = getOne(id);
        genderSalutionDAO.delete(genderSalution);
    }

    /**
     * Updates given genderSalution
     *
     * @param genderSalution The genderSalution to be updated
     */
    public void update(GenderSalution genderSalution){
        genderSalutionDAO.update(genderSalution);
    }

}
