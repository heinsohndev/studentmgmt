package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.CourseOfStudies;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for CourseOfStudies
 */
public class CourseOfStudiesDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all courseOfStudies that are currently in the database
     *
     * @return a list of all courseOfStudies that are currently in the database
     */
    @Transactional
    public List<CourseOfStudies> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select c from CourseOfStudies c",
                CourseOfStudies.class).getResultList();

    }


    /**
     * Returns the courseOfStudies identified by the given description
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public CourseOfStudies getOne(long id) {
        return sessionFactory.getCurrentSession().find(CourseOfStudies.class, id);
    }

    /**
     * Stores courseOfStudies into database
     *
     * @param courseOfStudies The courseOfStudies to be created
     */
    @Transactional
    public void create(CourseOfStudies courseOfStudies) {
        sessionFactory.getCurrentSession().save(courseOfStudies);
    }

    /**
     * Updates given courseOfStudies in the database
     *
     * @param courseOfStudies The courseOfStudies to be updates
     */
    @Transactional
    public void update(CourseOfStudies courseOfStudies) {
        sessionFactory.getCurrentSession().merge(courseOfStudies);
    }

    /**
     * Deletes given courseOfStudies
     *
     * @param courseOfStudies The courseOfStudies to be deleted
     */
    @Transactional
    public void delete(CourseOfStudies courseOfStudies) {
        sessionFactory.getCurrentSession().remove(courseOfStudies);
    }
}
