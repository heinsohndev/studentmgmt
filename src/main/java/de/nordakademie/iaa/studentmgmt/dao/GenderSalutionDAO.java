package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.GenderSalution;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for GenderSalution
 */
public class GenderSalutionDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all genderSalutions that are currently in the database
     *
     * @return a list of all genderSalutions that are currently in the database
     */
    @Transactional
    public List<GenderSalution> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select g from GenderSalution g",
                GenderSalution.class).getResultList();

    }


    /**
     * Returns the genderSalution identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public GenderSalution getOne(Long id) {
        return sessionFactory.getCurrentSession().find(GenderSalution.class, id);
    }

    /**
     * Stores genderSalution into database
     *
     * @param genderSalution The genderSalution to be created
     */
    @Transactional
    public void create(GenderSalution genderSalution) {
        sessionFactory.getCurrentSession().save(genderSalution);
    }

    /**
     * Updates given genderSalution in the database
     *
     * @param genderSalution The genderSalution to be updates
     */
    @Transactional
    public void update(GenderSalution genderSalution) {
        sessionFactory.getCurrentSession().merge(genderSalution);
    }

    /**
     * Deletes given genderSalution
     *
     * @param genderSalution The genderSalution to be deleted
     */
    @Transactional
    public void delete(GenderSalution genderSalution) {
        sessionFactory.getCurrentSession().remove(genderSalution);
    }

}
