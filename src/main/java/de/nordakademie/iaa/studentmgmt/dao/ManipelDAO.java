package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Manipel;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timi Gimm on 16.10.2018.
 *
 * DAO for Manipel
 */
public class ManipelDAO {

    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all manipels that are currently in the database
     *
     * @return a list of all manipels that are currently in the database
     */
    @Transactional
    public List<Manipel> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select m from Manipel m", Manipel.class).getResultList();

    }


    /**
     * Returns the manipel identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public Manipel getOne(Long id) {
        return sessionFactory.getCurrentSession().find(Manipel.class, id);
    }

    /**
     * Stores manipel into database
     *
     * @param manipel The manipel to be created
     */
    @Transactional
    public void create(Manipel manipel) {
        sessionFactory.getCurrentSession().save(manipel);
    }

    /**
     * Updates given manipel in the database
     *
     * @param manipel The manipel to be updates
     */
    @Transactional
    public void update(Manipel manipel) {
        sessionFactory.getCurrentSession().saveOrUpdate(manipel);
    }

    /**
     * Deletes given manipel
     *
     * @param manipel The manipel to be deleted
     */
    @Transactional
    public void delete(Manipel manipel) {
        sessionFactory.getCurrentSession().remove(manipel);
    }

}
