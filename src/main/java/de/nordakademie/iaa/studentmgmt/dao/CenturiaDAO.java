package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Centuria;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for Centuria
 */
public class CenturiaDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all centurias that are currently in the database
     *
     * @return a list of all centurias that are currently in the database
     */
    @Transactional
    public List<Centuria> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from Centuria", Centuria.class).list();

    }


    /**
     * Returns the centurias identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public Centuria getOne(Long id) {
        return sessionFactory.getCurrentSession().find(Centuria.class, id);
    }

    /**
     * Stores centuria into database
     *
     * @param centuria The centuria to be created
     */
    @Transactional
    public void create(Centuria centuria) {
        sessionFactory.getCurrentSession().save(centuria);
    }

    /**
     * Updates given centuria in the database
     *
     * @param centuria The centuria to be updates
     */
    @Transactional
    public void update(Centuria centuria) {
        sessionFactory.getCurrentSession().merge(centuria);
    }

    /**
     * Deletes given centuria
     *
     * @param centuria The centuria to be deleted
     */
    @Transactional
    public void delete(Centuria centuria) {
        sessionFactory.getCurrentSession().remove(centuria);
    }

}
