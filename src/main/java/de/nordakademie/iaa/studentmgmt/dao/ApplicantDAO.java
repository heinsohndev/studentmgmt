package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Applicant;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for Applicant
 */
@EnableTransactionManagement
public class ApplicantDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory)  {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all applicatns that are currently in the database
     *
     * @return a list of all applicants that are currently in the database
     */
    @Transactional
    public List<Applicant> getAll() {
       return sessionFactory.getCurrentSession().createQuery("select a from Applicant a", Applicant.class)
               .getResultList();
    }

    /**
     * Returns the applicant identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public Applicant getOne(Long id) {
        return sessionFactory.getCurrentSession().find(Applicant.class, id);
    }

    /**
     * Stores Applicant into database
     *
     * @param applicant The applicant to be created
     */
    @Transactional
    public void create(Applicant applicant)  {
        sessionFactory.getCurrentSession().save(applicant);
    }

    /**
     * Updates given applicant in the database
     *
     * @param applicant The applicant to be updates
     */
    @Transactional
    public void update(Applicant applicant) {
        sessionFactory.getCurrentSession().saveOrUpdate(applicant);
    }

    /**
     * Deletes given applicant
     *
     * @param applicant The applicant to be deleted
     */
    @Transactional
    public void delete(Applicant applicant) {
        sessionFactory.getCurrentSession().remove(applicant);
    }

}

