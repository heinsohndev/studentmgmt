package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Company;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for Company
 */
public class CompanyDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all companies that are currently in the database
     *
     * @return a list of all companies that are currently in the database
     */
    @Transactional
    public List<Company> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select c from Company c", Company.class).getResultList();

    }


    /**
     * Returns the company identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public Company getOne(Long id) {
        return sessionFactory.getCurrentSession().find(Company.class, id);
    }

    /**
     * Stores Company into database
     *
     * @param company The company to be created
     */
    @Transactional
    public void create(Company company) {
        sessionFactory.getCurrentSession().save(company);
    }

    /**
     * Updates given company in the database
     *
     * @param company The company to be updates
     */
    @Transactional
    public void update(Company company) {
        sessionFactory.getCurrentSession().merge(company);
    }

    /**
     * Deletes given company
     *
     * @param company The company to be deleted
     */
    @Transactional
    public void delete(Company company) {
        sessionFactory.getCurrentSession().remove(company);
    }

}
