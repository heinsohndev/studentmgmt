package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.City;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for City
 */
public class CityDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all cities that are currently in the database
     *
     * @return a list of all cities that are currently in the database
     */
    @Transactional
    public List<City> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select a from City a", City.class).getResultList();
    }


    /**
     * Returns the city identified by the given postCode
     *
     * @param postCode the identifier
     * @return the found entity
     */
    @Transactional
    public City getOne(Integer postCode) {
        return sessionFactory.getCurrentSession().find(City.class, postCode);
    }

    /**
     * Stores City into database
     *
     * @param city The city to be created
     */
    @Transactional
    public void create(City city) {
        sessionFactory.getCurrentSession().save(city);
    }

    /**
     * Updates given city in the database
     *
     * @param city The city to be updates
     */
    @Transactional
    public void update(City city) {
        sessionFactory.getCurrentSession().merge(city);
    }

    /**
     * Deletes given city
     *
     * @param city The city to be deleted
     */
    @Transactional
    public void delete(City city) {
        sessionFactory.getCurrentSession().remove(city);
    }


}
