package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Gender;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for gender
 */
public class GenderDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all gender that are currently in the database
     *
     * @return a list of all gender that are currently in the database
     */
    @Transactional
    public List<Gender> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select g from Gender g", Gender.class).getResultList();

    }


    /**
     * Returns the gender identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public Gender getOne(Long id) {
        return sessionFactory.getCurrentSession().find(Gender.class, id);
    }

    /**
     * Stores gender into database
     *
     * @param gender The gender to be created
     */
    @Transactional
    public void create(Gender gender) {
        sessionFactory.getCurrentSession().save(gender);
    }

    /**
     * Updates given gender in the database
     *
     * @param gender The gender to be updates
     */
    @Transactional
    public void update(Gender gender) {
        sessionFactory.getCurrentSession().merge(gender);
    }

    /**
     * Deletes given gender
     *
     * @param gender The gender to be deleted
     */
    @Transactional
    public void delete(Gender gender) {
        sessionFactory.getCurrentSession().remove(gender);
    }

}
