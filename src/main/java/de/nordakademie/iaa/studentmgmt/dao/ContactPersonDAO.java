package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.ContactPerson;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for ContactPerson
 */
public class ContactPersonDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all contactPersons that are currently in the database
     *
     * @return a list of all contactPersons that are currently in the database
     */
    @Transactional
    public List<ContactPerson> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select c from ContactPerson c", ContactPerson.class)
                .getResultList();

    }


    /**
     * Returns the contactPerson identified by the given id
     *
     * @param id the identifier
     * @return the found entity
     */
    @Transactional
    public ContactPerson getOne(Long id) {
        return sessionFactory.getCurrentSession().find(ContactPerson.class, id);
    }

    /**
     * Stores contactPerson into database
     *
     * @param contactPerson The contactPerson to be created
     */
    @Transactional
    public void create(ContactPerson contactPerson) {
        sessionFactory.getCurrentSession().save(contactPerson);
    }

    /**
     * Updates given contactPerson in the database
     *
     * @param contactPerson The contactPerson to be updates
     */
    @Transactional
    public void update(ContactPerson contactPerson) {
        sessionFactory.getCurrentSession().merge(contactPerson);
    }

    /**
     * Deletes given contactPerson
     *
     * @param contactPerson The contactPerson to be deleted
     */
    @Transactional
    public void delete(ContactPerson contactPerson) {
        sessionFactory.getCurrentSession().remove(contactPerson);
    }

}
