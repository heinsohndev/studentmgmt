package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.AcademicYear;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for AcademicYear
 */
public class AcademicYearDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all academicYears that are currently in the database
     *
     * @return a list of all academicYears that are currently in the database
     */
    @Transactional
    public List<AcademicYear> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select a from AcademicYear a", AcademicYear.class)
                .getResultList();

    }


    /**
     * Returns the academicYear identified by the given id
     *
     * @param year the identifier
     * @return the found entity
     */
    @Transactional
    public AcademicYear getOne(Short year) {
        return sessionFactory.getCurrentSession().find(AcademicYear.class, year);
    }

    /**
     * Stores academicYear into database
     *
     * @param academicYear The academicYear to be created
     */
    @Transactional
    public void create(AcademicYear academicYear) {
        sessionFactory.getCurrentSession().save(academicYear);
    }

    /**
     * Updates given academicYear in the database
     *
     * @param academicYear The academicYear to be updates
     */
    @Transactional
    public void update(AcademicYear academicYear) {
        sessionFactory.getCurrentSession().merge(academicYear);
    }

    /**
     * Deletes given academicYear
     *
     * @param academicYear The academicYear to be deleted
     */
    @Transactional
    public void delete(AcademicYear academicYear) {
        sessionFactory.getCurrentSession().remove(academicYear);
    }
}
