package de.nordakademie.iaa.studentmgmt.dao;

import de.nordakademie.iaa.studentmgmt.model.Centuria;
import de.nordakademie.iaa.studentmgmt.model.Manipel;
import de.nordakademie.iaa.studentmgmt.model.Student;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Timo Gimm on 16.10.2018.
 *
 * DAO for City
 */
@Component("studentDAO")
public class StudentDAO {
    /**
     * Current session factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * List of all students that are currently in the database
     *
     * @return a list of all student that are currently in the database
     */
    @Transactional
    public List<Student> getAll() {
        return sessionFactory.getCurrentSession().createQuery("select s from Student s", Student.class).getResultList();
    }


    /**
    * Returns the student identified by the given id
    *
    * @param matriculationNo the identifier
    * @return the found entity
    */
    @Transactional
    public Student getOne(Long matriculationNo) {
        return sessionFactory.getCurrentSession().find(Student.class, matriculationNo);
    }

    /**
     * Stores Student into database
     *
     * @param student The student to be created
     */
    @Transactional
    public void create(Student student) {
        sessionFactory.getCurrentSession().save(student);
    }

    /**
     * Updates given student in the database
     *
     * @param student The student to be updates
     */
    @Transactional
    public void update(Student student) {
        sessionFactory.getCurrentSession().merge(student);
    }

    /**
     * Deletes given student
     *
     * @param student The student to be deleted
     */
    @Transactional
    public void delete(Student student) {
        sessionFactory.getCurrentSession().remove(student);
    }

    /**
     * Gets all students by manipel
     * @param manipel manipel
     * @return list of students
     */
    @Transactional
    public List<Student> getAllByManipel(Manipel manipel) {
        return sessionFactory.getCurrentSession().createQuery(
                "select student from Student as student join student.centuria as centuria" +
                        " join student.centuria.manipel as manipel" +
                        " where manipel.id = :manipel_id  AND student.status = 1", Student.class)
                .setParameter("manipel_id",manipel.getId()).getResultList();
    }

    /**
     * Gets all students by centuria
     * @param centuria centuria
     * @return list of students
     */
    @Transactional
    public List<Student> getAllByCenturia(Centuria centuria) {
        return sessionFactory.getCurrentSession().createQuery(
                "select student from Student as student join student.centuria as centuria" +
                        " WHERE centuria.id = :centuria_id AND student.status = 1", Student.class)
                .setParameter("centuria_id",centuria.getId()).getResultList();
    }
}
