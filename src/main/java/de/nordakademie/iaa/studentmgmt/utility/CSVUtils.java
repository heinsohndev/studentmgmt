package de.nordakademie.iaa.studentmgmt.utility;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Utility class for creating a csv
 */
public class CSVUtils {


    private static final char DEFAULT_SEPARATOR = ',';

    /**
     * Writes a line into csv
     * @param w includes file writer
     * @param values String to write
     * @throws IOException exception if file can't be opened/written
     */
    public static void writeLine(Writer w, List<String> values) throws IOException {
        writeLine(w, values, DEFAULT_SEPARATOR, ' ');
    }

    /**
     * Writes a line into csv
     * @param w includes file writer
     * @param values String to write
     * @param separators Seperator into string
     * @throws IOException exception if file can't be opened/written
     */
    public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
        writeLine(w, values, separators, ' ');
    }

    /**
     * Follows CSV format
     * @param value String to format
     * @return formatted string
     */
    private static String followCVSformat(String value) {
        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    /**
     * Writes line into csv
     * @param w includes file writer
     * @param values strings to write
     * @param separators seperator used in string list
     * @param customQuote custom Quote
     * @throws IOException exception if file can't be opened/written
     */
    private static void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {

        boolean first = true;


        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }

            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());


    }

}
