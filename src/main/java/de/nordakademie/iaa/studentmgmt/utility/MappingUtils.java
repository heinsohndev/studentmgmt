package de.nordakademie.iaa.studentmgmt.utility;

import de.nordakademie.iaa.studentmgmt.model.Applicant;
import de.nordakademie.iaa.studentmgmt.model.Student;

/**
 * Utility class for mapping applicant to student
 */
public class MappingUtils {

    /**
     * Method to map applicant to student
     * @param applicant applicant to map
     * @param student student to create
     */
    public void mapApplicantToStudent(Applicant applicant, Student student) {
        student.setSurName(applicant.getSurName());
        student.setName(applicant.getName());
        student.setGenderSalution(applicant.getGenderSalution());
        student.setStreet(applicant.getStreet());
        student.setAdressAdd(applicant.getAdressAdd());
        student.setCity(applicant.getCity());
        student.setDateOfBirth(applicant.getDateOfBirth());
        student.setPlaceOfBirth(applicant.getPlaceOfBirth());
        student.setTelNo(applicant.getTelNo());
        student.setEmail(applicant.getEmail());
        student.setStatus((short) 1);
        student.setCourseOfStudies(applicant.getCourseOfStudies());
    }
}
