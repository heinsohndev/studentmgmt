package de.nordakademie.iaa.studentmgmt.utility;

import de.nordakademie.iaa.studentmgmt.model.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * Search utility class
 */
public class StudentUtils {

    /**
     * Filters a list of students filtered by their matrikel number
     * @param students to be filtered
     * @param matrikelNo filtered by value
     */
    public void filterByMatrikelNo(List<Student> students, long matrikelNo){
        List<Student> remStudents = new ArrayList<>();
        for (Student s:students){
            if (s.getMatriculationNo() != matrikelNo) remStudents.add(s);
        }
        students.removeAll(remStudents);
    }

    /**
     * Filters a list of students filtered by their matrikel number
     * @param students to be filtered
     * @param surname filtered by value
     */
    public void filterBySurName(List<Student> students, String surname){
        List<Student> remStudents = new ArrayList<>();
        for (Student s:students){
            if (!s.getSurName().toLowerCase().contains(surname.toLowerCase())) remStudents.add(s);
        }
        students.removeAll(remStudents);
    }

    /**
     * Filters a list of students filtered by their name
     * @param students to be filtered
     * @param name filtered by value
     */
    public void filterByName(List<Student> students, String name){
        List<Student> remStudents = new ArrayList<>();
        for (Student s:students){
            if (!s.getName().toLowerCase().contains(name.toLowerCase())) remStudents.add(s);
        }
        students.removeAll(remStudents);
    }

    /**
     * Filters a list of students filtered by their course of studies
     * @param students to be filtered
     * @param id filtered by value
     */
    public void filterByCourseOfStudies(List<Student> students, long id){
        List<Student> remStudents = new ArrayList<>();
        for (Student s:students){
            if (s.getCourseOfStudies().getId() != id) remStudents.add(s);
        }
        students.removeAll(remStudents);
    }

    /**
     * Filters a list of students filtered by their status
     * @param students to be filtered
     * @param id filtered by value
     */
    public void filterByStatus(List<Student> students, long id){
        List<Student> remStudents = new ArrayList<>();
        for (Student s:students){
            if (s.getStatus() != id) remStudents.add(s);
        }
        students.removeAll(remStudents);
    }

}
