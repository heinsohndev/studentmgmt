package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class City {

    private String city;
    private Integer postCode;

    @Id
    public Integer getPostCode() {
        return postCode;
    }

    public void setPostCode(Integer postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
