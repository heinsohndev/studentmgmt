package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
@Table(name="Centuria")
public class Centuria {
    private Long id;
    private Manipel manipel;
    private String centuriaGroup; //group ist ein Keyword, deswegen Centuria Group

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Manipel getManipel() {
        return manipel;
    }

    public void setManipel(Manipel manipel) {
        this.manipel = manipel;
    }

    public String getCenturiaGroup() {
        return centuriaGroup;
    }

    public void setCenturiaGroup(String centuriaGroup) {
        this.centuriaGroup = centuriaGroup;
    }

    @Override
    public String toString() {
        return manipel.toString() + centuriaGroup;
    }
}
