package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class GenderSalution {
    private Long id;
    private Gender gender;
    private String salution;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getSalution() {
        return salution;
    }

    public void setSalution(String salution) {
        this.salution = salution;
    }

}
