package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class Manipel {
    private Long id;
    private CourseOfStudies courseOfStudies;
    private AcademicYear academicYear;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public CourseOfStudies getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(CourseOfStudies courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    @ManyToOne
    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    @Override
    public String toString() {
        return courseOfStudies.getCutDescr() + academicYear.getCutYear();
    }
}
