package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class AcademicYear {

    private Short year;
    private Short cutYear;

    @Id
    public Short getYear() {
        return year;
    }

    public void setYear(Short year) {
        this.year = year;
    }

    public Short getCutYear() {
        return cutYear;
    }

    public void setCutYear(Short cutYear) {
        this.cutYear = cutYear;
    }
}
