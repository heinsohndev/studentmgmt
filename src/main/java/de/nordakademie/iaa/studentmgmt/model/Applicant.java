package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class Applicant {
    private Long id;
    private String surName;
    private String name;
    private GenderSalution genderSalution;
    private String street;
    private String adressAdd;
    private City city;
    private Date dateOfBirth;
    private String placeOfBirth;
    private String telNo;
    private String email;
    private CourseOfStudies courseOfStudies;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public GenderSalution getGenderSalution() {
        return genderSalution;
    }

    public void setGenderSalution(GenderSalution genderSalution) {
        this.genderSalution = genderSalution;
    }

    @ManyToOne
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAdressAdd() {
        return adressAdd;
    }

    public void setAdressAdd(String adressAdd) {
        this.adressAdd = adressAdd;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @ManyToOne
    public CourseOfStudies getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(CourseOfStudies courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    /** Formats date into "DD-MM-YYYY"
     * @return formated date
     */
    public String formatDate() {
        return  this.dateOfBirth.toString().substring(8,10) + "." +
                this.dateOfBirth.toString().substring(5,7) + "." +
                this.dateOfBirth.toString().substring(0,4) ;
    }


}
