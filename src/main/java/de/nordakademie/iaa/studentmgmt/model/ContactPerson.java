package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
public class ContactPerson {
    private Long id;
    private String surName;
    private String name;
    private GenderSalution genderSalution;
    private String eMail;
    private String telNo;
    private String faxNo;

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return eMail;
    }

    public void setEmail(String eMail) {
        this.eMail = eMail;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public GenderSalution getGenderSalution() {
        return genderSalution;
    }

    public void setGenderSalution(GenderSalution genderSalution) {
        this.genderSalution = genderSalution;
    }
}
