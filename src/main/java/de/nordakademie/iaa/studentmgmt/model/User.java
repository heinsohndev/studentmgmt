package de.nordakademie.iaa.studentmgmt.model;

import javax.persistence.*;

/**
 * Created by Timo Gimm on 15.10.2018.
 *
 */
@Entity
class User {
    private Long id;
    private Student student;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
