package de.nordakademie.iaa.studentmgmt.model;

/**
 * Model class Status
 */
public class Status {
    private int id;
    private String description;

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Initializes class and values
     * @param id id
     * @param description description
     */
    public Status(int id, String description) {
        this.id = id;
        this.description = description;
    }

    /**
     * Get a description as string by id
     * @param id id
     * @return description
     */
    public static String getDescriptionById(int id) {
        switch (id) {
            case 1:
                return "Immatrikuliert";
            case 2:
                return "Exmatrikuliert";
            default:
                return "Unbekannt";
        }
    }

    @Override
    public String toString() {
        return "Status";
    }

}
