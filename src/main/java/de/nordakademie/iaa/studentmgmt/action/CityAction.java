package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.City;
import de.nordakademie.iaa.studentmgmt.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for city
 */
public class CityAction extends ActionSupport implements Preparable {

    @Autowired
    private CityService cityService;

    private City city;
    private List<City> cities;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    /**
     * Creates a city
     * @return value for struts
     */
    public String create() {
        cityService.create(city);
        return SUCCESS;
    }

    /**
     * Deletes a city
     * @return value for struts
     */
    public String delete() {
        try {
            cityService.delete(city.getPostCode());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a city
     * @return value for struts
     */
    public String update() {
        cityService.update(city);
        return SUCCESS;
    }


    /**
     * Shows all cities
     * @return value for struts
     */
    public String showAll() {
        cities = cityService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a city
     * @return value for struts
     */
    public String show() {
        city = cityService.getOne(city.getPostCode());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        cities = cityService.getAll();
    }
}
