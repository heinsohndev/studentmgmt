package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.AcademicYear;
import de.nordakademie.iaa.studentmgmt.service.AcademicYearService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for academic year
 */
public class AcademicYearAction extends ActionSupport implements Preparable {

    @Autowired
    private AcademicYearService academicYearService;

    private AcademicYear academicYear;
    private List<AcademicYear> academicYears;

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear AcademicYear) {
        this.academicYear = AcademicYear;
    }

    public List<AcademicYear> getAcademicYears() {
        return academicYears;
    }

    public void setAcademicYears(List<AcademicYear> academicYears) {
        this.academicYears = academicYears;
    }

    /**
     * Creates an academic year
     * @return value for struts
     */
    public String create() {
        academicYearService.create(academicYear);
        return SUCCESS;
    }

    /**
     * Deletes an academic year
     * @return value for struts
     */
    public String delete() {
        try {
            academicYearService.delete(academicYear.getYear());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates an academic year
     * @return value for struts
     */
    public String update() {
        academicYearService.update(academicYear);
        return SUCCESS;
    }

    /**
     * fills academic years list with all entities from service
     * @return value for struts
     */
    public String showAll() {
        academicYears = academicYearService.getAll();
        return SUCCESS;
    }

    /**
     * Shows one academic year
     * @return value for struts
     */
    public String show() {
        academicYear = academicYearService.getOne(academicYear.getYear());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        academicYears = academicYearService.getAll();
    }
}
