package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.CourseOfStudies;
import de.nordakademie.iaa.studentmgmt.service.CourseOfStudiesService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for course of studies
 */
public class CourseOfStudiesAction extends ActionSupport implements Preparable {

    @Autowired
    private CourseOfStudiesService courseOfStudiesService;

    private CourseOfStudies courseOfStudy;
    private List<CourseOfStudies> courseOfStudies;

    public CourseOfStudies getCourseOfStudy() {
        return courseOfStudy;
    }

    public void setCourseOfStudy(CourseOfStudies courseOfStudy) {
        this.courseOfStudy = courseOfStudy;
    }

    public List<CourseOfStudies> getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(List<CourseOfStudies> courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    /**
     * Creates a course of studies
     * @return value of struts
     */
    public String create() {
        courseOfStudiesService.create(courseOfStudy);
        return SUCCESS;
    }

    /**
     * Deletes a course of studies
     * @return value of struts
     */
    public String delete() {
        try {
            courseOfStudiesService.delete(courseOfStudy.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a course of studies
     * @return value of struts
     */
    public String update() {
        courseOfStudiesService.update(courseOfStudy);
        return SUCCESS;
    }


    /**
     * Shows all course of studies
     * @return value of struts
     */
    public String showAll() {
        courseOfStudies = courseOfStudiesService.getAll();
        return SUCCESS;
    }

    /**
     * Show a course of studies
     * @return value of struts
     */
    public String show() {
        courseOfStudy = courseOfStudiesService.getOne(courseOfStudy.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        courseOfStudies = courseOfStudiesService.getAll();
    }
}
