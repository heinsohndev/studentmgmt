package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.ContactPerson;
import de.nordakademie.iaa.studentmgmt.model.GenderSalution;
import de.nordakademie.iaa.studentmgmt.service.ContactPersonService;
import de.nordakademie.iaa.studentmgmt.service.GenderSalutionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for contact person
 */
public class ContactPersonAction extends ActionSupport implements Preparable {

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private GenderSalutionService genderSalutionService;

    private ContactPerson contactPerson;
    private List<ContactPerson> contactPersons;
    private List<GenderSalution> genderSalutions;

    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }

    public List<ContactPerson> getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(List<ContactPerson> contactPersons) {
        this.contactPersons = contactPersons;
    }

    public List<GenderSalution> getGenderSalutions() {
        return genderSalutions;
    }

    public void setGenderSalutions(List<GenderSalution> genderSalutions) {
        this.genderSalutions = genderSalutions;
    }

    /**
     * Creates contact person
     * @return values for struts
     */
    public String create() {
        contactPersonService.create(contactPerson);
        return SUCCESS;
    }

    /**
     * Deletes a contact person
     * @return values for struts
     */
    public String delete() {
        try {
            contactPersonService.delete(contactPerson.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a contact person
     * @return value for struts
     */
    public String update() {
        contactPersonService.update(contactPerson);
        return SUCCESS;
    }


    /**
     * Shows all contact persons
     * @return value for struts
     */
    public String showAll() {
        contactPersons = contactPersonService.getAll();
        return SUCCESS;
    }

    /**
     * Show a contact person
     * @return value for struts
     */
    public String show() {
        contactPerson = contactPersonService.getOne(contactPerson.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        contactPersons = contactPersonService.getAll();
        genderSalutions = genderSalutionService.getAll();
    }
}
