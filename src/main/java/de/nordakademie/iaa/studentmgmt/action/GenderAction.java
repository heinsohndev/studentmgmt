package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.Gender;
import de.nordakademie.iaa.studentmgmt.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for genders
 */
public class GenderAction extends ActionSupport implements Preparable {

    @Autowired
    private GenderService genderService;

    private Gender gender;
    private List<Gender> genders;

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public List<Gender> getGenders() {
        return genders;
    }

    public void setGenders(List<Gender> genders) {
        this.genders = genders;
    }

    /**
     * Creates a gender
     * @return value for struts
     */
    public String create() {
        genderService.create(gender);
        return SUCCESS;
    }

    /**
     * Deletes a gender
     * @return value for struts
     */
    public String delete() {
        try {
            genderService.delete(gender.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a gender
     * @return value for struts
     */
    public String update() {
        genderService.update(gender);
        return SUCCESS;
    }


    /**
     * Shows all genders
     * @return value of struts
     */
    public String showAll() {
        genders = genderService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a gender
     * @return value of struts
     */
    public String show() {
        gender = genderService.getOne(gender.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        genders = genderService.getAll();
    }
}
