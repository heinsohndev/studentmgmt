package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.AcademicYear;
import de.nordakademie.iaa.studentmgmt.model.CourseOfStudies;
import de.nordakademie.iaa.studentmgmt.model.Manipel;
import de.nordakademie.iaa.studentmgmt.model.Student;
import de.nordakademie.iaa.studentmgmt.service.AcademicYearService;
import de.nordakademie.iaa.studentmgmt.service.CourseOfStudiesService;
import de.nordakademie.iaa.studentmgmt.service.ManipelService;
import de.nordakademie.iaa.studentmgmt.service.StudentService;
import de.nordakademie.iaa.studentmgmt.utility.CSVUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for manipel Action
 */
public class ManipelAction extends ActionSupport implements Preparable {


    @Autowired
    private ManipelService manipelService;

    @Autowired
    private CourseOfStudiesService courseOfStudiesService;

    @Autowired
    private AcademicYearService academicYearService;

    @Autowired
    private StudentService studentService;

    private Manipel manipel;
    private List<Manipel> manipels;
    private List<CourseOfStudies> courseOfStudies;
    private List<AcademicYear> academicYears;

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    private InputStream fileInputStream;

    public Manipel getManipel() {
        return manipel;
    }

    public void setManipel(Manipel manipel) {
        this.manipel = manipel;
    }

    public List<Manipel> getManipels() {
        return manipels;
    }

    public void setManipels(List<Manipel> manipels) {
        this.manipels = manipels;
    }

    public List<CourseOfStudies> getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(List<CourseOfStudies> courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    public List<AcademicYear> getAcademicYears() {
        return academicYears;
    }

    public void setAcademicYears(List<AcademicYear> academicYears) {
        this.academicYears = academicYears;
    }

    /**
     * Creates a manipel
     * @return value for struts
     */
    public String create() {
        manipelService.create(manipel);
        return SUCCESS;
    }

    /**
     * Deletes a manipel
     * @return value for struts
     */
    public String delete() {
        try {
            manipelService.delete(manipel.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a manipel
     * @return value for struts
     */
    public String update() {
        manipelService.update(manipel);
        return SUCCESS;
    }


    /**
     * Shows all manipels
     * @return value for struts
     */
    public String showAll() {
        manipels = manipelService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a manipel
     * @return value for struts
     */
    public String show() {
        manipel = manipelService.getOne(manipel.getId());
        return SUCCESS;
    }

    /**
     * Exports a manipel into a .csv file
     * @return value for struts
     * @throws IOException exception if file can't be opened/written
     */
    public String exportCSV() throws IOException {
        Manipel m = new Manipel();
        m.setId(manipel.getId());
        String csvFile = "C:/studentmgmt/output/abc.csv";
        File f = new File(csvFile);
        f.getParentFile().mkdirs();
        f.createNewFile();
        FileWriter writer = new FileWriter(csvFile);
        List<Student> students = studentService.getAllByManipel(m);
        CSVUtils.writeLine(writer, Arrays.asList("Zenturie", "Matrikelnummer", "Name", "Vorname"));
        for (Student student : students) {
            ArrayList<String> manipelList = new ArrayList<>();
            manipelList.add(student.getCenturia().toString());
            manipelList.add(student.getMatriculationNo().toString());
            manipelList.add(student.getSurName());
            manipelList.add(student.getName());
            CSVUtils.writeLine(writer, manipelList, ',');
        }
        writer.flush();
        writer.close();
        fileInputStream = new FileInputStream(new File("C:/studentmgmt/output/abc.csv"));
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        courseOfStudies = courseOfStudiesService.getAll();
        academicYears = academicYearService.getAll();
    }
}
