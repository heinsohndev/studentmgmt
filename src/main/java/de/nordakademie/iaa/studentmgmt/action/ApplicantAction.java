package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.*;
import de.nordakademie.iaa.studentmgmt.service.*;
import de.nordakademie.iaa.studentmgmt.utility.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Class for applicant Action
 */
public class ApplicantAction extends ActionSupport implements Preparable {

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private GenderSalutionService genderSalutionService;

    @Autowired
    private CityService cityService;

    @Autowired
    private CourseOfStudiesService courseOfStudiesService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private CenturiaService centuriaService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private MappingUtils mappingUtils;

    private Applicant applicant;
    private List<Applicant> applicants;
    private List<GenderSalution> genderSalutions;
    private List<City> cities;
    private List<CourseOfStudies> courseOfStudies;
    private List<Centuria> centurias;
    private Student student;
    private List<Company> companies;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public List<Applicant> getApplicants() {
        return applicants;
    }

    public void setApplicants(List<Applicant> applicants) {
        this.applicants = applicants;
    }

    public List<GenderSalution> getGenderSalutions() {
        return genderSalutions;
    }

    public void setGenderSalutions(List<GenderSalution> genderSalutions) {
        this.genderSalutions = genderSalutions;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<CourseOfStudies> getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(List<CourseOfStudies> courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Centuria> getCenturias() {
        return centurias;
    }

    public void setCenturias(List<Centuria> centurias) {
        this.centurias = centurias;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public void prepare() {
        genderSalutions = genderSalutionService.getAll();
        cities = cityService.getAll();
        courseOfStudies = courseOfStudiesService.getAll();
        centurias = centuriaService.getAll();
        companies = companyService.getAll();
    }

    /**
     * Creates an applicant
     * @return value for struts
     */
    public String create() {
        applicantService.create(applicant);
        return SUCCESS;
    }

    /**
     * Deletes an applicant
     * @return value for struts
     */
    public String delete() {
        applicantService.delete(applicant.getId());
        return SUCCESS;
    }

    /**
     * Updates an applicant
     * @return value for struts
     */
    public String update() {
        applicantService.update(applicant);
        return SUCCESS;
    }

    /**
     * Shows all applicants
     * @return value for struts
     */
    public String showAll() {
        applicants = applicantService.getAll();
        return SUCCESS;
    }

    /**
     * Shows one applicants
     * @return value for struts
     */
    public String show() {
        applicant = applicantService.getOne(applicant.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    /**
     * Matriculates one applicant and converts applicant into student
     * @return value for struts
     */
    public String matriculate() {
        applicant = applicantService.getOne(applicant.getId());
        mappingUtils.mapApplicantToStudent(applicant, student);
        studentService.create(student);
        applicantService.delete(applicant.getId());

        return SUCCESS;
    }
}
