package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.dao.*;
import de.nordakademie.iaa.studentmgmt.model.*;
import de.nordakademie.iaa.studentmgmt.service.CenturiaService;
import de.nordakademie.iaa.studentmgmt.service.CompanyService;
import de.nordakademie.iaa.studentmgmt.service.StudentService;
import de.nordakademie.iaa.studentmgmt.utility.StudentUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for student
 */
public class StudentAction extends ActionSupport implements Preparable {

    @Autowired
    private StudentService studentService;

    @Autowired
    private GenderSalutionDAO genderSalutionDAO;

    @Autowired
    private CityDAO cityDAO;

    @Autowired
    private CourseOfStudiesDAO courseOfStudiesDAO;

    @Autowired
    private CenturiaService centuriaService;

    @Autowired
    private StudentUtils studentUtils;

    @Autowired
    private CompanyService companyService;

    private Student student;
    private List<Student> students;
    private List<GenderSalution> genderSalutions;
    private List<City> cities;
    private List<CourseOfStudies> courseOfStudies;
    private long searchFieldMatrikelNo;
    private String searchFieldSurName;
    private String searchFieldName;
    private long searchFieldCourseOfStudies;
    private long searchFieldStatus;
    private List<Status> status;
    private int statusValue;
    private List<Company> companies;
    private List<Centuria> centurias;

    public int getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(int statusValue) {
        this.statusValue = statusValue;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<GenderSalution> getGenderSalutions() {
        return genderSalutions;
    }

    public void setGenderSalutions(List<GenderSalution> genderSalutions) {
        this.genderSalutions = genderSalutions;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public List<CourseOfStudies> getCourseOfStudies() {
        return courseOfStudies;
    }

    public void setCourseOfStudies(List<CourseOfStudies> courseOfStudies) {
        this.courseOfStudies = courseOfStudies;
    }

    public long getSearchFieldMatrikelNo() {
        return searchFieldMatrikelNo;
    }

    public void setSearchFieldMatrikelNo(long searchFieldMatrikelNo) {
        this.searchFieldMatrikelNo = searchFieldMatrikelNo;
    }

    public String getSearchFieldSurName() {
        return searchFieldSurName;
    }

    public void setSearchFieldSurName(String searchFieldSurName) {
        this.searchFieldSurName = searchFieldSurName;
    }

    public String getSearchFieldName() {
        return searchFieldName;
    }

    public void setSearchFieldName(String searchFieldName) {
        this.searchFieldName = searchFieldName;
    }

    public long getSearchFieldCourseOfStudies() {
        return searchFieldCourseOfStudies;
    }

    public void setSearchFieldCourseOfStudies(long searchFieldCourseOfStudies) {
        this.searchFieldCourseOfStudies = searchFieldCourseOfStudies;
    }

    public long getSearchFieldStatus() {
        return searchFieldStatus;
    }

    public void setSearchFieldStatus(long searchFieldStatus) {
        this.searchFieldStatus = searchFieldStatus;
    }

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

    public List<Centuria> getCenturias() {
        return centurias;
    }

    public void setCenturias(List<Centuria> centurias) {
        this.centurias = centurias;
    }

    public void prepare() {
        genderSalutions = genderSalutionDAO.getAll();
        cities = cityDAO.getAll();
        courseOfStudies = courseOfStudiesDAO.getAll();
        status = studentService.getAllStatus();
        companies = companyService.getAll();
        centurias = centuriaService.getAll();
    }


    /**
     * Creates a student
     * @return value for struts
     */
    public String create() {
        studentService.create(student);
        return SUCCESS;
    }

    /**
     * Exmactriculates a student
     * @return value for struts
     */
    public String exmatriculate() {
        studentService.exmatriculate(student.getMatriculationNo());
        return SUCCESS;
    }

    /**
     * Updates a student
     * @return value for struts
     */
    public String update() {
        studentService.update(student);
        return SUCCESS;
    }


    /**
     * Shows all students
     * @return value for struts
     */
    public String showAll() {
        students = studentService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a student
     * @return value for struts
     */
    public String show() {
        student = studentService.getOne(student.getMatriculationNo());
        return SUCCESS;
    }

    /**
     * Search function with optional parameters depending on filled out criteria
     * @return value for struts
     */
    public String search() {
        students = studentService.getAll();
        if (students.size() < 1) return SUCCESS;
        if (searchFieldMatrikelNo > 0) studentUtils.filterByMatrikelNo(students,searchFieldMatrikelNo);
        if (!searchFieldSurName.equals("")) studentUtils.filterBySurName(students,searchFieldSurName);
        if (!searchFieldName.equals("")) studentUtils.filterByName(students,searchFieldName);
        if (searchFieldCourseOfStudies > -1) studentUtils.filterByCourseOfStudies(students, searchFieldCourseOfStudies);
        if (searchFieldStatus > -1) studentUtils.filterByStatus(students, searchFieldStatus);

        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }
}
