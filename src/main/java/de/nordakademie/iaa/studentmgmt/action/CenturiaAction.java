package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.Centuria;
import de.nordakademie.iaa.studentmgmt.model.Manipel;
import de.nordakademie.iaa.studentmgmt.model.Student;
import de.nordakademie.iaa.studentmgmt.service.CenturiaService;
import de.nordakademie.iaa.studentmgmt.service.ManipelService;
import de.nordakademie.iaa.studentmgmt.service.StudentService;
import de.nordakademie.iaa.studentmgmt.utility.CSVUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Action class for centuria
 */
public class CenturiaAction extends ActionSupport implements Preparable {

    @Autowired
    private ManipelService manipelService;

    @Autowired
    private CenturiaService centuriaService;

    @Autowired
    private StudentService studentService;


    private Centuria centuria;
    private List<Manipel> manipels;
    private List<Centuria> centurias;
    private InputStream fileInputStream;

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public Centuria getCenturia() {
        return centuria;
    }

    public void setCenturia(Centuria centuria) {
        this.centuria = centuria;
    }

    public List<Manipel> getManipels() {
        return manipels;
    }

    public void setManipels(List<Manipel> manipels) {
        this.manipels = manipels;
    }

    public List<Centuria> getCenturias() {
        return centurias;
    }

    public void setCenturias(List<Centuria> centurias) {
        this.centurias = centurias;
    }


    /**
     * Creates one centuria
     * @return value for struts
     */
    public String create() {
        centuriaService.create(centuria);
        return SUCCESS;
    }

    /**
     * Deletes one centuria
     * @return value for struts
     */
    public String delete() {
        try {
            centuriaService.delete(centuria.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates one centuria
     * @return value for struts
     */
    public String update() {
        centuriaService.update(centuria);
        return SUCCESS;
    }


    /**
     * Shows all centuria
     * @return value for struts
     */
    public String showAll() {
        centurias = centuriaService.getAll();
        return SUCCESS;
    }

    /**
     * Shows one centuria
     * @return value for struts
     */
    public String show() {
        centuria = centuriaService.getOne(centuria.getId());
        return SUCCESS;
    }

    /**
     * Exports centuria into csv and offers a download
     * @return value for struts
     * @throws IOException exception if file can't be opened/written
     */
    public String exportCSV() throws IOException {
        Centuria c = new Centuria();
        c.setId(centuria.getId());
        String csvFile = "C:/studentmgmt/output/abc.csv";
        File f = new File(csvFile);
        f.getParentFile().mkdirs();
        f.createNewFile();
        FileWriter writer = new FileWriter(csvFile);
        List<Student> students = studentService.getAllByCenturia(c);
        CSVUtils.writeLine(writer, Arrays.asList("Zenturie", "Matrikelnummer", "Name", "Vorname"));
        for (Student student : students) {
            ArrayList<String> manipelList = new ArrayList<>();
            manipelList.add(student.getCenturia().toString());
            manipelList.add(student.getMatriculationNo().toString());
            manipelList.add(student.getSurName());
            manipelList.add(student.getName());
            CSVUtils.writeLine(writer, manipelList, ',');
        }
        writer.flush();
        writer.close();
        fileInputStream = new FileInputStream(new File("C:/studentmgmt/output/abc.csv"));
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        centurias = centuriaService.getAll();
        manipels = manipelService.getAll();
    }
}
