package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.Gender;
import de.nordakademie.iaa.studentmgmt.model.GenderSalution;
import de.nordakademie.iaa.studentmgmt.service.GenderSalutionService;
import de.nordakademie.iaa.studentmgmt.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for gender salution
 */
public class GenderSalutionAction extends ActionSupport implements Preparable {

    @Autowired
    private GenderSalutionService genderSalutionService;

    @Autowired
    private GenderService genderService;

    private GenderSalution genderSalution;
    private List<GenderSalution> genderSalutions;
    private List<Gender> genders;

    public GenderSalution getGenderSalution() {
        return genderSalution;
    }

    public void setGenderSalution(GenderSalution genderSalution) {
        this.genderSalution = genderSalution;
    }

    public List<GenderSalution> getGenderSalutions() {
        return genderSalutions;
    }

    public void setGenderSalutions(List<GenderSalution> genderSalutions) {
        this.genderSalutions = genderSalutions;
    }

    public List<Gender> getGenders() {
        return genders;
    }

    public void setGenders(List<Gender> genders) {
        this.genders = genders;
    }

    /**
     * Creates a gender salution
     * @return value for struts
     */
    public String create() {
        genderSalutionService.create(genderSalution);
        return SUCCESS;
    }

    /**
     * Deletes a gender salution
     * @return value of struts
     */
    public String delete() {
        try {
            genderSalutionService.delete(genderSalution.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a gender salution
     * @return value of struts
     */
    public String update() {
        genderSalutionService.update(genderSalution);
        return SUCCESS;
    }


    /**
     * Shows all gender salution
     * @return value of struts
     */
    public String showAll() {
        genderSalutions = genderSalutionService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a gender salution
     * @return value of struts
     */
    public String show() {
        genderSalution = genderSalutionService.getOne(genderSalution.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        genderSalutions = genderSalutionService.getAll();
        genders = genderService.getAll();
    }
}
