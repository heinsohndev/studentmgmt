package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import de.nordakademie.iaa.studentmgmt.model.City;
import de.nordakademie.iaa.studentmgmt.model.Company;
import de.nordakademie.iaa.studentmgmt.model.ContactPerson;
import de.nordakademie.iaa.studentmgmt.service.CityService;
import de.nordakademie.iaa.studentmgmt.service.CompanyService;
import de.nordakademie.iaa.studentmgmt.service.ContactPersonService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Action class for company
 */
public class CompanyAction extends ActionSupport implements Preparable {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ContactPersonService contactPersonService;

    @Autowired
    private CityService cityService;

    private Company company;
    private List<Company> companies;
    private List<ContactPerson> contactPersons;
    private List<City> cities;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company Company) {
        this.company = Company;
    }

    public List<Company> getcompanies() {
        return companies;
    }

    public void setcompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public List<ContactPerson> getContactPersons() {
        return contactPersons;
    }

    public void setContactPersons(List<ContactPerson> contactPersons) {
        this.contactPersons = contactPersons;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    /**
     * Creates a company
     * @return value for struts
     */
    public String create() {
        companyService.create(company);
        return SUCCESS;
    }

    /**
     * Deletes a company
     * @return value for struts
     */
    public String delete() {
        try {
            companyService.delete(company.getId());
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Updates a company
     * @return value for struts
     */
    public String update() {
        companyService.update(company);
        return SUCCESS;
    }


    /**
     * Shows all companies
     * @return value for struts
     */
    public String showAll() {
        companies = companyService.getAll();
        return SUCCESS;
    }

    /**
     * Shows a company
     * @return value for struts
     */
    public String show() {
        company = companyService.getOne(company.getId());
        return SUCCESS;
    }

    public String execute() {
        return SUCCESS;
    }

    @Override
    public void prepare() {
        companies = companyService.getAll();
        contactPersons = contactPersonService.getAll();
        cities = cityService.getAll();
    }
}
