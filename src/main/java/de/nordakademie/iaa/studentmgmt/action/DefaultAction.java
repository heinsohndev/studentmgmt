package de.nordakademie.iaa.studentmgmt.action;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Action class for default access of site
 */
public class DefaultAction extends ActionSupport {
    public String execute() {
        return SUCCESS;
    }
}
