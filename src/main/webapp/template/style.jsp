<style>
@import url('https://fonts.googleapis.com/css?family=Lato:400,700');
html {
font-family:'Lato',sans-serif;
color:#313131;
}
 #container {
                margin-left: auto;
                margin-right: auto;
                width: 100%;

            }
            nav {
                float:left;
            }
            header {
                padding-left:40px;
             }
           nav>ul {
                list-style: none;
                margin-top: 0px;
            }
            nav>ul>li {
                background: #aad5fc;
                border: 1px solid black;
                border-radius: 3px;
                padding: 5px;
                margin: 2.5px;
            }
            nav>ul>li>a {
                color: black;
                text-decoration: none;
            }
            table {
                border: 1px solid black;
                margin-top: 5px;
            }
            th {
                border-bottom:2px solid #131313;
            }
            td {
                border-left: 1px solid black;
            }
            .button {
                background: #aad5fc;
                border: 1px solid black;
                border-radius: 3px;
                color: black;
                text-decoration: none;
                padding: 1px;
                margin: 2px;
            }
            .button>a {
                color: black;
                text-decoration: none;
            }
            .createButton {
                margin-top:5px;
            }
            footer {
                clear: both;
            }
</style>