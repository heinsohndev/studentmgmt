<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<nav>
    <ul>
        <li><s:a namespace="/applicant" action="show" >Bewerber</s:a></li>
        <li><s:a namespace="/student" action="show" >Studenten</s:a></li>
        <li><s:a namespace="/city" action="show" >PLZ/Ort</s:a></li>
        <li><s:a namespace="/gender" action="show" >Geschlecht</s:a></li>
        <li><s:a namespace="/salution" action="show" >Anrede</s:a></li>
        <li><s:a namespace="/courseofstudies" action="show" >Studiengänge</s:a></li>
        <li><s:a namespace="/academicyear" action="show" >Studienjahrgang</s:a></li>
        <li><s:a namespace="/contactperson" action="show" >Kontaktperson</s:a></li>
        <li><s:a namespace="/company" action="show" >Kooperationsunternehmen</s:a></li>
        <li><s:a namespace="/manipel" action="show" >Manipel</s:a></li>
        <li><s:a namespace="/centuria" action="show" >Zenturien</s:a></li>

    </ul>
</nav>

