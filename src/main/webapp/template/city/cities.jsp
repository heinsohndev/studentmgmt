<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Stadt</th>
            <th>PLZ</th>
            <th></th>
            <th></th>
            <s:iterator value="cities">
                <tr>
                    <td>
                        <s:property value="city"/>
                    </td>
                    <td>
                        <s:property value="postCode"/>
                    </td>
                    <td>
                        <s:a namespace="/city" action="delete" class="button">
                            <s:param name="city.postCode" value="postCode"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/city" action="single" class="button">
                            <s:param name="city.postCode" value="postCode"/>
                            Update
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/city" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
