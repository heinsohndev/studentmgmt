<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:textfield name="city.city" label="Stadt" value="%{city.city}"/>
            <s:textfield name="city.postCode" label="PLZ" value="%{city.postCode}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
