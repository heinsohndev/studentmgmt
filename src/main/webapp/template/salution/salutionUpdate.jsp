<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="genderSalution.id" value="%{genderSalution.id}" />
            <s:textfield name="genderSalution.salution" label="Anrede" value="%{genderSalution.salution}"/>
            <s:select name="genderSalution.gender.id" list="genders" headerValue="---Select---" headerKey="-1" key="Geschlecht" listKey="id" listValue="gender"/>
            <s:submit/>
        </s:form>

    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
