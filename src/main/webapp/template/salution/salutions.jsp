<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Anrede</th>
            <th>Geschlecht</th>
            <th></th>
            <th></th>
            <s:iterator value="genderSalutions">
                <tr>
                    <td>
                        <s:property value="salution"/>
                    </td>
                    <td>
                        <s:property value="gender.gender"/>
                    </td>
                    <td>
                        <s:a namespace="/salution" action="delete" class="button">
                            <s:param name="genderSalution.id" value="id"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/salution" action="single" class="button">
                            <s:param name="genderSalution.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/salution" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
