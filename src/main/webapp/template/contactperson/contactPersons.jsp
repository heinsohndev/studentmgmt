<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Vorname</th>
            <th>Nachname</th>
            <th>Anrede</th>
            <th>E-Mail</th>
            <th>Telefon</th>
            <th>Fax</th>
            <th></th>
            <th></th>
            <s:iterator value="contactPersons">
                <tr>
                    <td>
                        <s:property value="name"/>
                    </td>
                    <td>
                        <s:property value="surName"/>
                    </td>
                    <td>
                        <s:property value="genderSalution.salution"/>
                    </td>
                    <td>
                        <s:property value="email"/>
                    </td>
                    <td>
                        <s:property value="telNo"/>
                    </td>
                    <td>
                        <s:property value="faxNo"/>
                    </td>
                    <td>
                        <s:a namespace="/contactperson" action="delete" class="button">
                            <s:param name="contactPerson.id" value="id"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/contactperson" action="single" class="button">
                            <s:param name="contactPerson.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/contactperson" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
