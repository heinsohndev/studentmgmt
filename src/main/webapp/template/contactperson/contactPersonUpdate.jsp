<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="contactPerson.id" value="%{contactPerson.id}"/>
            <s:textfield name="contactPerson.surName" label="Nachname" value="%{contactPerson.surName}"/>
            <s:textfield name="contactPerson.name" label="Vorname" value="%{contactPerson.name}"/>
            <s:select name="contactPerson.genderSalution.id" list="genderSalutions" headerValue="---Select---"
                      headerKey="-1" key="Anrede" listKey="id" listValue="salution"/>
            <s:textfield name="contactPerson.email" label="E-Mail" value="%{contactPerson.email}"/>
            <s:textfield name="contactPerson.telNo" label="Telefon" value="%{contactPerson.telNo}"/>
            <s:textfield name="contactPerson.faxNo" label="Fax" value="%{contactPerson.faxNo}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
