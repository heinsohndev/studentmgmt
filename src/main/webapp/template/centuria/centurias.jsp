<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>ID</th>
            <th>Klasse</th>
            <th>Manipel</th>
            <th></th>
            <th></th>
            <th></th>
            <s:iterator value="centurias">
                <tr>
                    <td>
                        <s:property value="%{toString()}"/>
                    </td>
                    <td>
                        <s:property value="centuriaGroup"/>
                    </td>
                    <td>
                        <s:property value="%{manipel.toString()}"/>
                    </td>
                    <td>
                        <s:a namespace="/centuria" action="delete" class="button">
                            <s:param name="centuria.id" value="id"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/centuria" action="single" class="button">
                            <s:param name="centuria.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/centuria" action="create-csv" class="button">
                            <s:param name="centuria.id" value="id"/>
                            Ergebnisliste
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/centuria" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
