<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="create">
            <s:textfield name="applicant.name" label="Vorname"/>
            <s:textfield name="applicant.surName" label="Nachname"/>
            <s:select name="applicant.genderSalution.id" list="genderSalutions" headerValue="---Select---"
                      headerKey="-1" key="Anrede" listKey="id" listValue="salution"/>
            <s:textfield name="applicant.street" label="Straße"/>
            <s:textfield name="applicant.adressAdd" label="Hausnummer"/>
            <s:select name="applicant.city.postCode" list="cities" headerValue="---Select---" headerKey="-1" key="Stadt"
                      listKey="postCode" listValue="city"/>
            <s:textfield name="applicant.dateOfBirth" label="Geburtsdatum"/>
            <s:textfield name="applicant.placeOfBirth" label="Geburtsort"/>
            <s:textfield name="applicant.telNo" label="Telefonnummer"/>
            <s:textfield name="applicant.email" label="E-Mail"/>
            <s:select name="applicant.courseOfStudies.id" list="courseOfStudies" headerValue="---Select---"
                      headerKey="-1" key="Studienkurs" listKey="id" listValue="description"/>
            <s:select name="student.company.id" list="companies" headerValue="---Select---" headerKey="-1"
                                  key="Unternehmen" listKey="id" listValue="name" value="%{student.company.id}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
