<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="applicant.id" value="%{applicant.id}"/>
            <s:textfield name="applicant.name" label="Vorname" value="%{applicant.name}"/>
            <s:textfield name="applicant.surName" label="Nachname" value="%{applicant.surName}"/>
            <s:select name="applicant.genderSalution.id" list="genderSalutions" headerValue="---Select---"
                      headerKey="-1" key="Anrede" listKey="id" listValue="salution"
                      value="%{applicant.genderSalution.id}"/>
            <s:textfield name="applicant.street" label="Straße" value="%{applicant.street}"/>
            <s:textfield name="applicant.adressAdd" label="Hausnummer" value="%{applicant.adressAdd}"/>
            <s:select name="applicant.city.postCode" list="cities" headerValue="---Select---" headerKey="-1" key="Stadt"
                      listKey="postCode" listValue="city" value="%{applicant.city.postCode}"/>
            <s:textfield name="applicant.dateOfBirth" label="Geburtsdatum" value="%{applicant.formatDate()}"/>
            <s:textfield name="applicant.placeOfBirth" label="Geburtsort" value="%{applicant.placeOfBirth}"/>
            <s:textfield name="applicant.telNo" label="Telefonnummer" value="%{applicant.name}"/>
            <s:textfield name="applicant.email" label="E-Mail" value="%{applicant.email}"/>
            <s:select name="applicant.courseOfStudies.id" list="courseOfStudies" headerValue="---Select---"
                      headerKey="-1" key="Studienkurs" listKey="id" listValue="description"
                      value="%{applicant.courseOfStudies.id}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
