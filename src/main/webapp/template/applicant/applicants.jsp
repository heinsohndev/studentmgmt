<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Nachname</th>
            <th>Vorname</th>
            <th>Anrede</th>
            <th>Straße</th>
            <th>Hausnummer</th>
            <th>Stadt</th>
            <th>Geburtsdatum</th>
            <th>Geburtsort</th>
            <th>Telefon</th>
            <th>E-Mail</th>
            <th>Studienkurs</th>
            <th></th>
            <th></th>
            <th></th>
            <s:iterator value="applicants">
                <tr>
                    <td>
                        <s:property value="name"/>
                    </td>
                    <td>
                        <s:property value="surName"/>
                    </td>
                    <td>
                        <s:property value="genderSalution.salution"/>
                    </td>
                    <td>
                        <s:property value="street"/>
                    </td>
                    <td>
                        <s:property value="adressAdd"/>
                    </td>
                    <td>
                        <s:property value="city.city"/>
                    </td>
                    <td>
                        <s:property value="%{formatDate()}"/>
                    </td>
                    <td>
                        <s:property value="placeOfBirth"/>
                    </td>
                    <td>
                        <s:property value="telNo"/>
                    </td>
                    <td>
                        <s:property value="email"/>
                    </td>
                    <td>
                        <s:property value="courseOfStudies.description"/>
                    </td>
                    <td>
                        <s:a namespace="/applicant" action="delete-form" class="button">
                            <s:param name="applicant.id" value="id"/>
                            Löschen
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/applicant" action="single" class="button">
                            <s:param name="applicant.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/applicant" action="matriculate-form" class="button">
                            <s:param name="applicant.id" value="id"/>
                            Immatrikulieren
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/applicant" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
