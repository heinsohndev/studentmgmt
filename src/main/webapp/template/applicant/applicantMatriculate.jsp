<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="matriculate">
            <s:hidden value="%{applicant.id}" name="applicant.id"/>
            <s:select name="student.centuria.id" list="centurias" headerValue="---Zenturie---" headerKey="-1"
                      key="Zenturie" listKey="id" listValue="%{toString()}"/>
            <s:select name="student.company.id" list="companies" headerValue="---Select---" headerKey="-1"
                      key="Unternehmen" listKey="id" listValue="name" value="%{student.company.id}"/>
            <s:submit value="Immatrikulieren"/>
        </s:form>

    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
