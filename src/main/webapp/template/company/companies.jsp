<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Name</th>
            <th>Name Zusatz</th>
            <th>Kürzel</th>
            <th>Straße</th>
            <th>Stadt</th>
            <th>Hausnummer</th>
            <th>Kontaktperson</th>
            <th></th>
            <th></th>
            <s:iterator value="companies">
                <tr>
                    <td>
                        <s:property value="name"/>
                    </td>
                    <td>
                        <s:property value="name2"/>
                    </td>
                    <td>
                        <s:property value="cutName"/>
                    </td>
                    <td>
                        <s:property value="street"/>
                    </td>
                    <td>
                        <s:property value="city.city"/>
                    </td>
                    <td>
                        <s:property value="adressAdd"/>
                    </td>
                    <td>
                        <s:property value="contactPerson.surName"/>
                    </td>
                    <td>
                        <s:a namespace="/company" action="delete" class="button">
                            <s:param name="company.id" value="id"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/company" action="single" class="button">
                            <s:param name="company.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/company" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
