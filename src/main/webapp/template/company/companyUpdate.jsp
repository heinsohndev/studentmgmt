<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="company.id" value="%{company.id}"/>
            <s:textfield name="company.name" label="Name" value="%{company.name}"/>
            <s:textfield name="company.name2" label="Name Zusatz" value="%{company.name2}"/>
            <s:textfield name="company.cutName" label="Kürzel" value="%{company.cutName}"/>
            <s:textfield name="company.street" label="Straße" value="%{company.street}"/>
            <s:textfield name="company.adressAdd" label="Hausnummer" value="%{company.adressAdd}"/>
            <s:select name="company.city.postCode" list="cities" headerValue="---Select---" headerKey="-1" key="Stadt"
                      listKey="postCode" listValue="City" value="%{company.city.postCode}"/>
            <s:select name="company.contactPerson.id" list="contactPersons" headerValue="---Select---" headerKey="-1"
                      key="Kontaktperson" listKey="id" listValue="surName" value="%{company.contactPerson.id}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
