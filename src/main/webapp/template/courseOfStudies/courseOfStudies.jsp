<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>Bezeichnung</th>
            <th>Abkürzung</th>
            <th></th>
            <th></th>
            <s:iterator value="courseOfStudies">
                <tr>
                    <td>
                        <s:property value="description"/>
                    </td>
                    <td>
                        <s:property value="cutDescr"/>
                    </td>
                    <td>
                        <s:a namespace="/courseofstudies" action="delete" class="button">
                            <s:param name="courseOfStudy.id" value="id"/>
                            Löschen
                        </s:a>
                        <s:a namespace="/courseofstudies" action="single" class="button">
                            <s:param name="courseOfStudy.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/courseofstudies" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
