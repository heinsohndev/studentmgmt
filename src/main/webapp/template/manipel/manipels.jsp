<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <table>
            <th>ID</th>
            <th>Akademisches Jahr</th>
            <th>Studienkurs</th>
            <th></th>
            <th></th>
            <th></th>
            <s:iterator value="manipels">
                <tr>
                    <td>
                        <s:property value="%{toString()}"/>
                    </td>
                    <td>
                        <s:property value="academicYear.year"/>
                    </td>
                    <td>
                        <s:property value="courseOfStudies.description"/>
                    </td>
                    <td>
                        <s:a namespace="/manipel" action="delete" class="button">
                            <s:param name="manipelId" value="id"/>
                            Löschen
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/manipel" action="single" class="button">
                            <s:param name="manipel.id" value="id"/>
                            Update
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/manipel" action="create-csv" class="button">
                            <s:param name="manipel.id" value="id"/>
                            Ergebnisliste
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/manipel" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
