<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="manipel.id" value="%{manipel.id}"/>
            <s:select name="manipel.courseOfStudies.id" list="courseOfStudies" headerValue="---Select---" headerKey="-1"
                      key="Studienkurs" listKey="id" listValue="description" value="%{manipel.courseOfStudies.id}"/>
            <s:select name="manipel.academicYear.year" list="academicYears" headerValue="---Select---" headerKey="-1"
                      key="Akademisches Jahr" listKey="year " listValue="year" value="%{manipel.academicYear.year}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
