<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>

</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="update">
            <s:hidden name="student.matriculationNo" value="%{student.matriculationNo}"/>
            <s:textfield name="student.name" label="Vorname" value="%{student.name}"/>
            <s:textfield name="student.surName" label="Nachname" value="%{student.surName}"/>
            <s:select name="student.genderSalution.id" list="genderSalutions" headerValue="---Select---" headerKey="-1"
                      key="Anrede" listKey="id" listValue="salution" value="%{student.genderSalution.id}"/>
            <s:textfield name="student.street" label="Straße" value="%{student.street}"/>
            <s:textfield name="student.adressAdd" label="Hausnummer" value="%{student.adressAdd}"/>
            <s:select name="student.city.postCode" list="cities" headerValue="---Select---" headerKey="-1" key="Stadt"
                      listKey="postCode" listValue="city" value="%{student.city.postCode}"/>
            <s:textfield name="student.dateOfBirth" label="Geburtsdatum" value="%{student.formatDate()}"/>
            <s:textfield name="student.placeOfBirth" label="Geburtsort" value="%{student.placeOfBirth}"/>
            <s:textfield name="student.telNo" label="Telefonnummer" value="%{student.telNo}"/>
            <s:textfield name="student.email" label="E-Mail" value="%{student.email}"/>
            <s:select name="student.centuria.id" list="centurias" headerValue="---Select---" headerKey="-1"
                      key="Zenturie" listKey="id" listValue="%{toString()}" value="%{student.centuria.id}"/>
            <s:select name="student.courseOfStudies.id" list="courseOfStudies" headerValue="---Select---" headerKey="-1"
                      key="Studienkurs" listKey="id" listValue="description" value="%{student.courseOfStudies.id}"/>
            <s:select name="student.status" list="status" headerValue="---Select---" headerKey="-1"
                      key="Immatrikuliert/Exmatrikuliert" listKey="id" listValue="description"
                      value="%{student.status}"/>
            <s:select name="student.company.id" list="companies" headerValue="---Select---" headerKey="-1"
                      key="Unternehmen" listKey="id" listValue="name" value="%{student.company.id}"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
