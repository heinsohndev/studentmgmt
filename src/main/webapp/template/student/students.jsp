<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form theme="simple" action="search" style="margin-bottom: 0px">
            <s:textfield name="searchFieldMatrikelNo" value="" placeholder="Matrikel-Nr"/>
            <s:textfield name="searchFieldSurName" placeholder="Nachname" value=""/>
            <s:textfield name="searchFieldName" placeholder="Vorname" value=""/>
            <s:select name="searchFieldCourseOfStudies" list="courseOfStudies" headerValue="Studiengang" headerKey="-1"
                      key="Studiengang" listKey="id" listValue="description"/>
            <s:select name="searchFieldStatus" list="status" headerValue="Status" headerKey="-1"
                      key="Immatrikuliert/Exmatrikuliert" listKey="id" listValue="description"/>
            <s:submit value="Suchen"/>
        </s:form>
        <table>
            <th>Matrikel-Nr.</th>
            <th>Nachname</th>
            <th>Vorname</th>
            <th>Anrede</th>
            <th>Straße</th>
            <th>Hausnummer</th>
            <th>Stadt</th>
            <th>Geburtdatum</th>
            <th>Geburtsort</th>
            <th>Telefon</th>
            <th>E-Mail</th>
            <th>Studienkurs</th>
            <th>Zenturie</th>
            <th>Unternehmen</th>
            <th>Betreuer</th>
            <th>Status</th>
            <th></th>
            <th></th>
            <th></th>
            <s:iterator value="students">
                <tr>
                    <td>
                        <s:property value="matriculationNo"/>
                    </td>
                    <td>
                        <s:property value="surName"/>
                    </td>
                    <td>
                        <s:property value="name"/>
                    </td>
                    <td>
                        <s:property value="genderSalution.salution"/>
                    </td>
                    <td>
                        <s:property value="street"/>
                    </td>
                    <td>
                        <s:property value="adressAdd"/>
                    </td>
                    <td>
                        <s:property value="city.city"/>
                    </td>
                    <td>
                        <s:property value="%{formatDate()}"/>
                    </td>
                    <td>
                        <s:property value="placeOfBirth"/>
                    </td>
                    <td>
                        <s:property value="telNo"/>
                    </td>
                    <td>
                        <s:property value="email"/>
                    </td>
                    <td>
                        <s:property value="courseOfStudies.description"/>
                    </td>
                    <td>
                        <s:property value="%{centuria.toString()}"/>
                    </td>
                    <td>
                        <s:property value="company.name"/>
                    </td>
                    <td>
                        <s:property value="company.contactPerson.surName"/>
                    </td>
                    <td>
                        <s:property value="%{statusToString()}"/>
                    </td>
                    <td>
                        <s:a namespace="/student" action="single" class="button">
                            <s:param name="student.matriculationNo" value="matriculationNo"/>
                            Update
                        </s:a>
                    </td>
                    <td>
                        <s:a namespace="/student" action="exmatriculate" class="button">
                            <s:param name="student.matriculationNo" value="matriculationNo"/>
                            Exmatrikulieren
                        </s:a>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <div class="createButton">
            <s:a namespace="/student" action="create-form" class="button">
                Erstellen
            </s:a>
        </div>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
