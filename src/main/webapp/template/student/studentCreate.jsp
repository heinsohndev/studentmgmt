<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>
    <div id="content">
        <s:form action="create">
            <s:textfield name="student.name" label="Vorname"/>
            <s:textfield name="student.surName" label="Nachname"/>
            <s:select name="student.genderSalution.id" list="genderSalutions" headerValue="---Select---" headerKey="-1"
                      key="Anrede" listKey="id" listValue="salution"/>
            <s:textfield name="student.street" label="Straße"/>
            <s:textfield name="student.adressAdd" label="Hausnummer"/>
            <s:select name="student.city.postCode" list="cities" headerValue="---Select---" headerKey="-1" key="Stadt"
                      listKey="postCode" listValue="city"/>
            <s:textfield name="student.dateOfBirth" label="Geburtsdatum"/>
            <s:textfield name="student.placeOfBirth" label="Geburtsort"/>
            <s:textfield name="student.telNo" label="Telefonnummer"/>
            <s:textfield name="student.email" label="E-Mail"/>
            <s:select name="student.centuria.id" list="centurias" headerValue="---Select---" headerKey="-1"
                      key="Zenturie" listKey="id" listValue="%{toString()}"/>
            <s:select name="student.courseOfStudies.id" list="courseOfStudies" headerValue="---Select---" headerKey="-1"
                      key="Studienkurs" listKey="id" listValue="description"/>
            <s:select name="student.company.id" list="companies" headerValue="---Select---" headerKey="-1"
                                  key="Unternehmen" listKey="id" listValue="name" value="%{student.company.id}"/>
            <s:select name="student.status" list="status" headerValue="---Select---" headerKey="-1"
                      key="Immatrikuliert/Exmatrikuliert" listKey="id" listValue="description"/>
            <s:submit/>
        </s:form>
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
