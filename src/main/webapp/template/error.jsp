<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <tiles:insertAttribute name="style"/>
</head>
<body>
<div id="container">
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="navbar"/>

    <div id="content">
        Es ist ein Fehler aufgetreten. Bitte stellen Sie sicher, dass kein Eintrag auf diese Referenztabelle zugreift.
    </div>
</div>
</body>
<tiles:insertAttribute name="footer"/>
</html>
